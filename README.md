# Swi-Prolog
Zeki sistemler dersi kapsamında ufak bir yer edinen swi-prolog ile ilgili notlarım ve uygulamalarımın olduğu depo.

#### Ufak notlar
+ Prolog programları cümleciklerden oluşur. 3 tür cümlecik vardır.
  + Facts(Gerçekler): Daima doğru olan şeyleri tanımlar.
  + Rules(Kurallar) : Koşul veya koşullara bağlı olarak doğru olan şeyleri tanımlar.
  + Questions(Sorgular): Kullanıcının veritabanını sorgulamak için kurduğu cümleciklerdir.
+ Her cümleciğin sonuna mutlaka nokta (.) konur.
+ Değişkenler büyük harf veya altçizgi (_) ile başlamak zorundadır.
+ İki tür sayı tipi kullanılabilir.
  + Integer(Tamsayılar)
  + Float(Ondalıklı sayılar)
+ İsimlendirilmemiş değişkenlere **anonim** değişken denir. Sadece altçizgi ile belirtilir ve çıkış değeri olarak verilemezler.

##### Devamını vakit buldukça ekleyeceğim.
